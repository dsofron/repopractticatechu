import { LitElement, html, customElement } from 'lit-element';

class PersonaHeader extends LitElement {
    constructor(){
        super();
    }

    static get properties(){
        return {

        };
    }
    
    render()  {
        return html`
        <h1>Persona header</h1>
        `;
    }
}

customElements.define('persona-header', PersonaHeader);