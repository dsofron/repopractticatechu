import { LitElement, html, customElement } from 'lit-element';

class PersonaFooter extends LitElement {
    constructor(){
        super();
    }

    static get properties(){
        return {

        };
    }

    render()  {
        return html`
        <h4>@Persona footer</h4>
        `;
    }
}

customElements.define('persona-footer', PersonaFooter);