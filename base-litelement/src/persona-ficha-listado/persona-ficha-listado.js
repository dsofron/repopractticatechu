import { LitElement, html, customElement } from 'lit-element';

class PersonaFichaListado extends LitElement {
    constructor(){
        super();
    }

    static get properties(){
        return {
            fname:{type:String},
            years:{type:String},
            profile:{type:String},
            photo: {type:Object}
        };
    }
    
    render()  {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        
        <div class="card h-100">
            <img src=${this.photo.src} alt=${this.photo.alt} 
            height="100" width="100" class="card-img-top"/>
        
            <div class="card-body">
                <h5 class="card-title">${this.fname}</h5>

                <p5>${this.profile}</p5>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">${this.years} años en la empresa</li>
                </ul>
            </div>

            <div class="card-footer">
            <button class="btn btn-danger col-5" @click=${this.deletePerson}><strong>X</strong></button>
            <button class="btn btn-info col-5 offset-1" @click=${this.moreInfo}><strong>Info</strong></button>
		    </div>
        
            </div>
        </div>
        `;
    }

    moreInfo(e){
        console.log("more info en persona-ficha-listado");
        console.log("se va modificar la persona "+this.fname);


        this.dispatchEvent(
            new CustomEvent("info-person", {
                detail: {
                    name: this.fname
                }
            })

        );
    }

    deletePerson(e){
        console.log("delete person en persona-ficha-listado");
        console.log("se va borrar la persona "+this.fname);

        this.dispatchEvent(
            new CustomEvent("delete-person", {
                detail: {
                    name: this.fname
                }
            })

        );
    }
}

customElements.define('persona-ficha-listado', PersonaFichaListado);