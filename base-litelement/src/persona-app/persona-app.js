import { LitElement, html, customElement } from 'lit-element';

import '../persona-footer/persona-footer.js'
import '../persona-stats/persona-stats.js'

class PersonaApp extends LitElement {
    static get properties(){
        return {
            people: {type:Array}
        }
    }

    constructor(){
        super();

        this.people = [];
    }
    render()  {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        
        <persona-header></persona-header>

        <div class="row" >
            <persona-sidebar  @new-person="${this.newPerson}" class="col-2"></persona-sidebar>
            <persona-main @updated-people="${this.updatePeople}" class="col-10"></persona-main>
        </div>

        <persona-footer></persona-footer>
        <persona-stats @updated-people-stats = "${this.peopleStatsUpdated}"></persona-stats>
        `;
    }

    peopleStatsUpdated(e){
        console.log("peopleStatsUpdated persona-app");
        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
    }

    newPerson(e){
        console.log("new person en persona-app");
        console.log("se va a crear una nueva persona");

        this.shadowRoot.querySelector("persona-main").showForm = true;
    }

    updatePeople(e){
        console.log("update people en persona-app");
        console.log(e.detail.people);
        this.people = e.detail.people;
    }

    updated(changedProperties){
        console.log("updated en persona-app");

        if (changedProperties.has("people")){
            console.log("ha cambiado el valor de la propiedad people en persona-app");
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }
}

customElements.define('persona-app', PersonaApp);