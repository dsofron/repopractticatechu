import { LitElement, html, css, customElement } from 'lit-element';

import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'

class PersonaMain extends LitElement {
    constructor(){
        super();

        this.people = [
           {name: "Ellen Ripley", years: "34", 
           photo:{"src":"./img/persona.jpg", "alt":"Ellen Ripley"},
           profile: "Lorem ipsum Lorem ipsum."
        },
           {name: "Emma", years: "9",
           photo:{"src":"./img/persona.jpg", "alt":"Emma"},
           profile: "Lorem ipsum Lorem ipsum Emma."
        },
           {name:  "Bruce lenny", years: "5",
           photo:{"src":"./img/persona.jpg", "alt":"Bruce lenny"},
           profile: "Lorem ipsum Lorem ipsum Bruce lenny."
        },
           {name:  "Person 1", years: "6",
           photo:{"src":"./img/persona.jpg", "alt":"Person 1"},
           profile: "Lorem ipsum Lorem ipsum Person 1."
           },
           {name:  "Person 2", years: "23",
           photo:{"src":"./img/persona.jpg", "alt":"Person 2"},
           profile: "Lorem ipsum Lorem ipsum Persn 2."
           }
        ];

        this.showPersonList = false
    }

    static get styles(){
        return css` 
            :host {
                all:initial;
            }
        `;

    }

    static get properties(){
        return {
            people:{type:Array},
            showForm:{type:Boolean}
        };
    }

    render()  {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <h2 class="class-center">Personas</h2>
        <div class="row" id="peopleList">
        <div class="row row-cols-1 row-cols-sm-4">
            ${this.people.map(
                person => html`<persona-ficha-listado 
                fname="${person.name}" 
                years="${person.years}"
                @delete-person="${this.deletePerson}"
                @info-person="${this.infoPerson}"
                profile="${person.profile}"
                .photo="${person.photo}">
                 
                </persona-ficha-listado>`
            )}
            </div>
        </div>

        <div class="row">
            <persona-form id="personForm" class="d-none border rounded border-rounded" 
            @persona-form-close="${this.personFormClose}"
            @persona-form-store="${this.storePerson}"></persona-form>
        </div>
        `;
    }

    storePerson(e){
        console.log("store person en persona-main");
        console.log(e.detail.person.name);
        console.log(e.detail.person.profile);
        console.log(e.detail.person.years);

        let person =  {name: e.detail.person.name, years: e.detail.person.years,
                      photo:e.detail.person.photo,
                      profile: e.detail.person.profile
                      }

                      console.log(person);

        if (e.detail.editingPerson == true){
            console.log("se va a actualizar la persona :" + person.name)

            this.people.map(
                person => person.name === e.detail.person.name
                ? e.detail.person : person 
            );
        }

        else{
            console.log("se va a almacenar una nueva persona :" + person.name);
            //this.people.push(person);
            this.people = [...this.people, e.detail.person];
        }

        this.showForm = false;
        this.personFormClose();
        this.showList()
    }

    personFormClose(){
        console.log("Se ha cerrado persona form");

        this.showForm = false;
    }

    //se ejecuta cuando LitElement detecta un cambio en las propiedades
    updated(changedProperties){
        console.log("changedProperties");

        if (changedProperties.has("showForm")){
            console.log("ha cambiado el valor de la propiedad showPersonList en persona-main");

            if  (this.showForm ===  true){
                console.log("true");
                this.showPersonForm();
            }
            else{
                console.log("false");
                this.showList();
            }
        }

        if (changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad en la persona-main");
            this.dispatchEvent(
                new CustomEvent("updated-people", {
                    detail: {people: this.people}
                })
            )
        }
    }

    showList(){
        console.log("show person list");
        console.log("mostrando el listado de  personas");

        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    showPersonForm(){
        console.log("show person form");
        console.log("mostrando el formulario de alta de persona");

        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
    }

    deletePerson(e){
        console.log("persona main");
        console.log(e.detail.name);
        console.log(this.people);

        this.people = this.people.filter(
            person => person.name != e.detail.name
        );

        console.log(this.people);
    }

    infoPerson(e){
        console.log("persona main");
        console.log("Se ha pedido información de : " + e.detail.name);

        let choosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        this.shadowRoot.getElementById("personForm").person = choosenPerson[0];
        console.log(choosenPerson[0])
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showForm = true;
    }
}

customElements.define('persona-main', PersonaMain);