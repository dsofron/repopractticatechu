import { LitElement, html, customElement } from 'lit-element';

class PersonaForm extends LitElement {
    static get properties(){
        return {
            person: {type: Object},
            editingPerson: {type:Boolean}
        };
    }

    constructor(){
        super();
        
        this.resetFormData();
    }

    render()  {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
       
        <div>
            <form>
                <div class="form-group">
                    <label>Nombre completo</label>
                    <input @input="${this.updateName}" type="text" id="personFormName" .value="${this.person.name}" 
                    ?disabled="${this.editingPerson}" class="form-control" placeholder="Nombre completo" />
                </div>

                <div class="form-group">
                    <label>Perfil</label>
                    <textarea class="form-control" placeholder="Perfil" rows="5" .value="${this.person.profile}" @input="${this.updateProfile}"></textarea>
                </div>

                <div class="form-group">
                    <label>Años en la empresa</label>
                    <input @input="${this.updateYears}" type="text" class="form-control" .value="${this.person.years}"  placeholder="Años en la empresa" />
                </div>

                <button class="btn btn-primary" @click=${this.goBack}><strong>Atrás</strong></button>
                <button class="btn btn-success" @click=${this.storePerson}><strong>Guardar</strong></button>
            </form>
        </div>
        `;
    }

    updateName(e){
        console.log("update name con el valor "  + e.target.value);
        this.person.name = e.target.value;
    }

    updateProfile(e){
        console.log("update profile con el valor "  + e.target.value);
        this.person.profile = e.target.value;
    }

    updateYears(e){
        console.log("update years con el valor "  + e.target.value);
        this.person.years = e.target.value;
    }

    storePerson(e){
        console.log("store person");
        e.preventDefault();

        console.log("nombre persona "+ this.person.name);
        console.log("profile persona "+ this.person.profile);
        console.log("nombre years "+ this.person.years);

        this.person.photo = {
            "src" : "./img/persona.jpg",
            "alt" : "Persona"
        }

        this.dispatchEvent(new CustomEvent("persona-form-store",  {
            detail: {
                person : {
                    name : this.person.name,
                    profile : this.person.profile,
                    years : this.person.years,
                    photo: this.person.photo
                },
                editingPerson: this.editingPerson
            }
        }))
    }

    goBack(e){
        console.log("go back");
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("persona-form-close", {}));
        this.resetFormData();
    }

    resetFormData(){
        console.log("reset form data");
        this.person = {};
        this.person.name = "";
        this.person.years = "";
        this.person.profile = "";

        this.editingPerson = false;
    }
}

customElements.define('persona-form', PersonaForm);