import { LitElement, html, customElement } from 'lit-element';

class FichaPersona extends LitElement {
    static get properties() {
        return{
        name: {type: String},
        years: {type: Number},
        personInfo: {type: String},
        photo: {type: Object}
        }
    }

    constructor(){
        super();

        this.name="Prueba Nombre";
        this.years=12;

        this.updateLevelPerson();

        this.photo = {
            src: "../img/persona.jpg",
            alt: "Foto persona"
        }
    }

    updated(changedProperties){
        changedProperties.forEach((oldValue, propName) =>  {
console.log("Propiedad " + propName + " cambia valor, anterior era  "  + oldValue);
        }

        );

        if (changedProperties.has("name")){
            console.log("Name cambia valor de " + changedProperties.get("name") + " a  "
            + this.name );
        }

        if (changedProperties.has("years")) {
            console.log("Propiedad yearsInCompany cambiada valor anterior era " + changedProperties.get("years") + " nuevo es " + this.yearsInCompany);
            this.updateLevelPerson();
        }
    }

    render()  {
        return html`
        <div>

        <label for="fname">Nombre Ciompleto</label>
        <input type="text" id="fname" name="fname" value="${this.name}"
        @input="${this.updateName}" ></name>
        <br />

        <label for="years">Años en la empresa</label>
        <input type="text" name="years" value="${this.year}"
        @input="${this.updateYearsInCompany}"></input>
        <br />

        <input type="text" name="personInfo" value="${this.personInfo}" disabled></input>
        <br />

        <img src="${this.photo.src}" height="200" width="200" 
        alt="${this.photo.alt}">

        </div>
        `;
    }

    updateName(e){
        //console.log("update name");
        this.name = e.target.value;
    }

    updateYearsInCompany(e){
        console.log("update years ");
        this.years = e.target.value;
        //this.updateLevelPerson();
    }

    updateLevelPerson(){

        if (this.years >= 7){
            this.personInfo = "lead";
        }
        else if (this.years >= 5){
            this.personInfo = "señor";
        }
        else if (this.years >= 3){
            this.personInfo = "team";
        }
        else {
            this.personInfo = "junior";
        }
    }
}

customElements.define('ficha-persona', FichaPersona);