import { LitElement, html, customElement } from 'lit-element';

class EmisorEvento extends LitElement {
    render()  {
        return html`
        <h3>Emisor evento</h3>
        <button @click="${this.sendEvent}">No pulsar</button>
        `;
    }

    sendEvent(e){
        console.log("sent event");
        console.log("Pulsado botón");

        this.dispatchEvent(
            new CustomEvent(
                "test-event",
                {
                    "detail": {
                        "course":"Tech UI",
                        "year": 2000
                    }
                }
            )
        )
    }
}

customElements.define('emisor-evento', EmisorEvento);