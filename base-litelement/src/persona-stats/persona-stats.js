import { LitElement, html, customElement } from 'lit-element';

class PersonaStats extends LitElement {

    static get properties(){
        return {
            people: {type:Array}
        }
    }

    constructor(){
        super();

        this.people = [];
    }

    updated(changedProperties){
        console.log("changed properties en persona-stats");

        if (changedProperties.has("people")){
            console.log("ha cambiado el valor de la propiedad people en persona-stats");
            
            let peopleStats = this.gatherPeopleArrayInfo(this.people);
            this.dispatchEvent(
                new CustomEvent("updated-people-stats",{
                    detail: {
                        peopleStats : peopleStats
                    }
                })
            )
        }
    }

    gatherPeopleArrayInfo(people){
        console.log("gatherPeopleArrayInfo");

        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;

        return peopleStats;
    }

    render()  {
        return html`
        <div>Persona Stats</div>
        `;
    }
}

customElements.define('persona-stats', PersonaStats);